<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$data = array();
		$data['script_links'] = script_tag('assets/plugins/jquery-knob/jquery.knob.js').script_tag('assets/plugins/morris/morris.min.js')
		.script_tag('assets/plugins/raphael/raphael-min.js').script_tag('assets/pages/jquery.dashboard.js');
		$this->layout->view('Dashboard/dashboard_view',$data);
	}
}
